package ru.t1.didyk.taskmanager.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class DataXmlSaveJaxBRequest extends AbstractUserRequest {
    public DataXmlSaveJaxBRequest(@Nullable String token) {
        super(token);
    }
}
