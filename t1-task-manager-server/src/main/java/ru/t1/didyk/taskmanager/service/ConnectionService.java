package ru.t1.didyk.taskmanager.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.api.service.IConnectionService;
import ru.t1.didyk.taskmanager.api.service.IDatabaseProperty;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IDatabaseProperty databaseProperty;

    public ConnectionService(@NotNull IDatabaseProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Connection getConnection() {
        @NotNull final String username = databaseProperty.getDatabaseUser();
        @NotNull final String password = databaseProperty.getDatabasePassword();
        @NotNull final String url = databaseProperty.getDatabaseUrl();
        @NotNull Connection connection = DriverManager.getConnection(url, username, password);
        connection.setAutoCommit(false);
        return connection;
    }
}
