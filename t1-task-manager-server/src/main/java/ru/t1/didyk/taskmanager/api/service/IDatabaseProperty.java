package ru.t1.didyk.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {

    @NotNull String getDatabaseUser();

    @NotNull String getDatabasePassword();

    @NotNull String getDatabaseUrl();

}
