package ru.t1.didyk.taskmanager.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.api.repository.IRepository;
import ru.t1.didyk.taskmanager.comparator.CreatedComparator;
import ru.t1.didyk.taskmanager.comparator.StatusComparator;
import ru.t1.didyk.taskmanager.model.AbstractModel;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final Connection connection;

    protected abstract String getTableName();

    @NotNull
    protected abstract M fetch(@NotNull final ResultSet row);

    public AbstractRepository(@NotNull Connection connection) {
        this.connection = connection;
    }

    @NotNull
    protected final String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == StatusComparator.INSTANCE) return "status";
        else return "name";
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String sql = String.format("delete from %s", getTableName());
        try {
            @NotNull final Statement statement = connection.createStatement();
            statement.executeUpdate(sql);
        } catch (Exception e) {
            throw e;
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("select * from %s", getTableName());
        try {
            @NotNull final Statement statement = connection.createStatement();
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next())
                result.add(fetch(resultSet));
        } catch (Exception e) {
            throw e;
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        /*@NotNull final List<M> result = new ArrayList<>(models);
        result.sort(comparator);*/
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("select * from %s order by %s", getTableName(), getSortType(comparator));
        try {
            @NotNull final Statement statement = connection.createStatement();
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next())
                result.add(fetch(resultSet));
        } catch (Exception e) {
            throw e;
        }
        return result;
    }

    @Nullable
    @Override
    public abstract M add(@Nullable final M model);

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@NotNull final String id) {
        /*return models
                .stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst().orElse(null);*/
        M result = null;
        @NotNull final String sql = String.format("select * from %s where id='%s' limit 1", getTableName(), id);
        try {
            @NotNull final Statement statement = connection.createStatement();
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next())
                result = fetch(resultSet);
        } catch (Exception e) {
            throw e;
        }
        return result;
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        return findAll().get(index);
    }

    @Override
    @SneakyThrows
    public int getSize() {
        int result = -1;
        @NotNull final String sql = String.format("select count(*) from %s", getTableName());
        try {
            @NotNull final Statement statement = connection.createStatement();
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next())
                result = resultSet.getInt("count");
        } catch (Exception e) {
            throw e;
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M remove(@Nullable final M model) {
        @NotNull final String sql = String.format("delete from %s where id='%s'", getTableName(), model.getId());
        try {
            @NotNull final Statement statement = connection.createStatement();
            statement.executeUpdate(sql);
        } catch (Exception e) {
            throw e;
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeById(@NotNull final String id) {
        @Nullable final M model = findOneById(id);
        if (model == null) return null;
        @NotNull final String sql = String.format("delete from %s where id='%s'", getTableName(), id);
        try {
            @NotNull final Statement statement = connection.createStatement();
            statement.executeUpdate(sql);
        } catch (Exception e) {
            throw e;
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeByIndex(@NotNull final Integer index) {
        @Nullable final M model = findOneByIndex(index);
        if (model == null) return null;
        return removeById(model.getId());
    }

    @Override
    public void removeAll(@Nullable final List<M> modelsRemove) {
        if (modelsRemove == null) return;
        modelsRemove.forEach(model -> remove(model));
    }

    @Override
    public @NotNull Collection<M> add(@NotNull Collection<M> models) {
        models.forEach(model -> add(model));
        return models;
    }

    @Override
    public @NotNull Collection<M> set(@NotNull Collection<M> models) {
        clear();
        return add(models);
    }
}
