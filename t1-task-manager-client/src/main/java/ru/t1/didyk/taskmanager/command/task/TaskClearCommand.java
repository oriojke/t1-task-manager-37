package ru.t1.didyk.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.dto.request.TaskClearRequest;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-clear";
    @NotNull
    public static final String DESCRIPTION = "Remove all tasks.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        @NotNull final TaskClearRequest request = new TaskClearRequest(getToken());
        serviceLocator.getTaskEndpointClient().clear(request);
    }

}
