package ru.t1.didyk.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.dto.request.TaskBindToProjectRequest;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-bind-to-project";
    @NotNull
    public static final String DESCRIPTION = "Bind task to project.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("[ENTER PROJECT ID:]");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID:]");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(getToken());
        request.setProjectId(projectId);
        request.setTaskId(taskId);
        serviceLocator.getTaskEndpointClient().bindToProject(request);
    }

}
